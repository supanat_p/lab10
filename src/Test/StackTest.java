package Test;

import static org.junit.Assert.*;

import java.util.EmptyStackException;

import ku.util.Stack;
import ku.util.StackFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StackTest {
	
	private Stack<String> stack;

	@Before
	public void setUp() throws Exception {
		StackFactory.setStackType( 0 );
	}

	@After
	public void tearDown() throws Exception {
		stack = null;
	}
	
	@Test
	public void testIsEmpty() {
		stack = StackFactory.makeStack(0);
		assertTrue( "Stack should empty" , stack.isEmpty() );
	}
	
	@Test
	public void testIsFull() {
		stack = StackFactory.makeStack(2);
		stack.push("banana");
		stack.push("apple");
		assertTrue( "Stack should full" , stack.isFull());
	}
	
	@Test
	public void testSizeWithEmptyStatck() {
		stack = StackFactory.makeStack(0);
		assertEquals( "Stack size should be zero" , 0 , stack.size());
	}
	
	@Test
	public void testSizeWhenPush() {
		stack = StackFactory.makeStack(2);
		assertEquals( "Stack size should be zero" , 0 , stack.size());
		stack.push("banana");
		assertEquals( "Stack size should be one" , 1 , stack.size());
	}
	
	@Test
	public void testCapacityWithEmptyStack() {
		stack = StackFactory.makeStack(0);
		assertEquals( "Stack capacity should be zero" , 0 , stack.capacity());
	}
	
	@Test( expected = IllegalArgumentException.class )
	public void testPushWithNull() {
		stack = StackFactory.makeStack(3);
		stack.push(null);
		fail("No object to push");
	}
	
	@Test
	public void testCapacityWithNormalStack() {
		stack = StackFactory.makeStack(3);
		assertEquals( "Stack capacity should be three" , 3 , stack.capacity());
		stack.push("banana");
		assertEquals( "Stack capacity should be three" , 3 , stack.capacity());
	}
	
	@Test( expected = EmptyStackException.class )
	public void testPopWithEmptyStack() {
		stack = StackFactory.makeStack(5);
		stack.pop();
		fail("No element to pop");
	}
	
	@Test
	public void testPopWithNormalStack() {
		stack = StackFactory.makeStack(3);
		stack.push("apple");
		stack.push("banana");
		stack.push("mango");
		assertEquals( "Should get mango" , "mango" ,stack.pop());
		assertEquals( "Should get banana" , "banana" , stack.pop());
	}
	
	@Test
	public void testPeekWithEmptyStack() {
		stack = StackFactory.makeStack(5);
		assertNull( "Should see null element" , stack.peek());
	}
	
	@Test
	public void testPeekWithNormalStack() {
		stack = StackFactory.makeStack(3);
		stack.push("apple");
		assertEquals( "Should see apple" , "apple" , stack.peek());
	}
	
	@Test( expected = IllegalStateException.class)
	public void testPushWithFullStack() {
		stack = StackFactory.makeStack(3);
		stack.push("banana");
		stack.push("apple");
		stack.push("mango");
		stack.push("orange");
		fail("Can't push when stack is full");
	}
	
	@Test
	public void testSizeWhenPop() {
		stack = StackFactory.makeStack(3);
		stack.push("banana");
		stack.push("apple");
		stack.push("mango");
		stack.pop();
		assertEquals( "Stack size should be two" , 2 , stack.size());
	}
	
	@Test
	public void testSizeWhenPopPush() {
		stack = StackFactory.makeStack(5);
		assertEquals( "Stack size should be zero" , 0 , stack.size());
		stack.push("banana");
	}
	
	
	
	
}
